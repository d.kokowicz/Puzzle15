//
//  RandomUIColor.swift
//  Puzzle15
//
//  Created by Dominika Kokowicz on 27/05/2021.
//

import UIKit

//MARK: Random color generator
extension UIColor {
  static var random: UIColor {
    return UIColor(red: .random(in: 0...2), green: .random(in: 0...2), blue: .random(in: 0...2), alpha: 1.0)
  }
}
