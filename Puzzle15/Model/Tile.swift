//
//  ModelTile.swift
//  Puzzle15
//
//  Created by Dominika Kokowicz on 27/05/2021.
//

import UIKit

struct Tile {
  let width: CGFloat
//  var xCenter: CGFloat? {
//    tileWidth / 2
//  }
//  var yCenter: CGFloat? {
//    tileWidth / 2
//  }
//  
  init(_ tileWidth: CGFloat) {
    self.width = tileWidth
  }
}
