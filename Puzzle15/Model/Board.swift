//
//  ModelBoard.swift
//  Puzzle15
//
//  Created by Dominika Kokowicz on 27/05/2021.
//

import UIKit

struct Board {
  var width: CGFloat
  let size = 4
  
  init(_ boardWidth: CGFloat) {
    self.width = boardWidth
  }
}
