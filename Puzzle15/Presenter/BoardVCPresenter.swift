//
//  BoardVCPresenter.swift
//  Puzzle15
//
//  Created by Dominika Kokowicz on 23/05/2021.
//

import UIKit

protocol BoardVCPresenterDelegate: AnyObject {
  func updateMovesLabel()
  func getLastPosition()
  func createBoardAndTiles()
  func modelsDidCreate(board: Board) -> BoardView
}

final class BoardVCPresenter {
        
  weak var delegate: BoardVCPresenterDelegate?
    
  var boardModel = Board(0)
  var tileModel = Tile(0)
  
  var initialTilesOrder = [Int]()
  
  var shuffledCentersOfTiles = [CGPoint]()
  var shuffledTilesOrder = [Int]()
  
  var tilesArray: NSMutableArray = []

  var shuffledTilesPosition = [(Int, CGPoint)]()
  
  var currentTilesOrder = [Int]()
    
  func calculateCGPointDistance(from: CGPoint, to: CGPoint) -> CGFloat {
    return sqrt((from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y))
  }
  
  func clearCurrentPosition() {
    shuffledTilesOrder.removeAll()
    shuffledTilesPosition.removeAll()
    shuffledCentersOfTiles.removeAll()
    currentTilesOrder.removeAll()
  }
  
  func isGameSolvable() -> Bool {
    let n = countInversion() + blankRowNumber()
    return n % 2 == 0
  }
  
  func countInversion() -> Int {
    var inversionCount = 0
    var inversionToCount = currentTilesOrder
    guard let blankIndex = currentTilesOrder.firstIndex(of: boardModel.size * boardModel.size) else { return 0 }
    inversionToCount[blankIndex] = 0
    
    for i in 0..<(boardModel.size * boardModel.size - 1) {
        for j in (i + 1)..<(boardModel.size * boardModel.size) {
            if inversionToCount[j] != 0 &&
                inversionToCount[i] != 0 &&
                inversionToCount[i] > inversionToCount[j] {
              inversionCount += 1
            }
        }
    }
    return inversionCount
  }
  
  func gameWon() -> Bool {
    initialTilesOrder == currentTilesOrder
  }
  
  func blankRowNumber() -> Int {
    guard let blankIndex = currentTilesOrder.firstIndex(of: boardModel.size * boardModel.size) else { return 0 }
    switch blankIndex {
    case 0...boardModel.size - 1:
      return 1
    case boardModel.size...2 * boardModel.size - 1:
      return 2
    case 2 * boardModel.size...3 * boardModel.size - 1:
      return 3
    case 3 * boardModel.size...4 * boardModel.size - 1:
      return 4
    default:
      return 0
    }
  }
  
  func removeEmptyTileFromBoard(_ image: UIImageView) {
    image.removeFromSuperview()
    tilesArray.remove(0)
  }
}

