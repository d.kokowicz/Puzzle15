//
//  Board.swift
//  Puzzle15
//
//  Created by Dominika Kokowicz on 23/05/2021.
//
//
import UIKit

final class BoardView: UIView {
  var modelBoard: Board
  
  init(modelBoard: Board, boardWidth: CGFloat) {
    self.modelBoard = Board(boardWidth)
    super.init(frame: CGRect(x: 0, y: 0, width: modelBoard.width, height: modelBoard.width))
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}




