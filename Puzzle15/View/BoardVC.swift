//
//  ViewController.swift
//  Puzzle15
//
//  Created by Dominika Kokowicz on 23/05/2021.
//

import UIKit

final class BoardVC: UIViewController {

  var tileWidth: CGFloat = 0
  
  var xCenter: CGFloat?
  var yCenter: CGFloat?
    
  var initialCentersOfTiles = [CGPoint]()
  
  var startingTilesPosition = [(Int, CGPoint)]() 
   
  var movesCount = 0
  
  var emptyTile: CGPoint?
  
  weak var board3: UIView!

  @IBOutlet weak var board: UIView!
  @IBOutlet weak var movesLabel: UILabel!
  @IBOutlet weak var timerLabel: UILabel!

  private let presenter = BoardVCPresenter()
  
  var seconds = 0
  var timer = Timer()
  
  @IBAction func shuffleButtonPressed(_ sender: UIBarButtonItem) {
    stopTimer()
    resetTimer()
    shuffleBoard()
    createTimer()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    presenter.delegate = self
    configureNavigationController()
    createTimer()
    startTimer()
    modelsDidCreate(board: presenter.boardModel)
  }

  
  override func viewDidAppear(_ animated: Bool) {
    createBoardAndTiles()
    configureLabels()
    shuffleBoard()
    getLastPosition()
    //createBoard()
  }
  
  @objc func shuffleBoard() {
    var centersCopy = initialCentersOfTiles
    for imageView in presenter.tilesArray {
      let randomIndex: Int = Int.random(in: 0...centersCopy.count - 1)
      let randomCenterLocation: CGPoint = centersCopy[randomIndex]
      
      (imageView as! UIImageView).center = randomCenterLocation
      centersCopy.remove(at: randomIndex)
  
      emptyTile = (imageView as! UIImageView).viewWithTag(16)?.center
    }
    movesCount = 0
    movesLabel.text = "Moves: \(movesCount)"
  }
    
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let myTouch = touches.first else { return }
    let tapCenter = myTouch.view!.center
    
    if myTouch.view != self.view {
      let distance = presenter.calculateCGPointDistance(from: emptyTile ?? CGPoint(x: 0, y: 0), to: tapCenter)
      if distance == tileWidth {
    
        updateMovesLabel()
 
        UIView.animate(withDuration: 0.3) { [self] in
          myTouch.view?.center = emptyTile ?? CGPoint(x: 0, y: 0)
          self.board.backgroundColor = .random
        }
        emptyTile = tapCenter
      }
    }
    presenter.clearCurrentPosition()
    getLastPosition()
   
    if presenter.isGameSolvable() != true {
      presentAlertNotSolvable()
      movesCount = 0
      seconds = 0
    }
    if presenter.gameWon() {
      presentAlertWon()
    }
  }
  
  func presentAlertWon() {
    let ac = UIAlertController(title: "Congratulations! You won!",
                               message: "Moves: \(movesCount). Time: \(timerLabel.text!)",
                               preferredStyle: .alert)
    addActionAndPresentAlert(alert: ac)
  }
  
  func presentAlertNotSolvable() {
    let ac = UIAlertController(title: "Your game is unsolvable. Reshuffle!",
                               message: "Moves: \(movesCount). Time: \(timerLabel.text!)",
                               preferredStyle: .alert)
    addActionAndPresentAlert(alert: ac)
  }
}

//MARK: Presenter delegate
extension BoardVC: BoardVCPresenterDelegate {
  func modelsDidCreate(board: Board) -> BoardView {
    let boardView = BoardView(modelBoard: board, boardWidth: board.width)
    return boardView
  }
  
  
  func updateMovesLabel() {
    movesCount += 1
    movesLabel.text = "Moves: \(movesCount)"
  }
  
  func getLastPosition() {
    for image in self.board.subviews as [UIView] {
      presenter.shuffledTilesOrder.append(image.tag)
      presenter.shuffledCentersOfTiles.append(image.center)
    }
    presenter.shuffledTilesOrder.append(presenter.shuffledTilesOrder.count + 1)
    guard let emptyTile = emptyTile else { return }
    presenter.shuffledCentersOfTiles.append(emptyTile)
    presenter.shuffledTilesPosition = Array(zip(presenter.shuffledTilesOrder, presenter.shuffledCentersOfTiles))
    
    presenter.shuffledTilesPosition.sort { $0.1.x < $1.1.x }
    presenter.shuffledTilesPosition.sort { $0.1.y < $1.1.y }
    presenter.shuffledTilesPosition.map { presenter.currentTilesOrder.append($0.0) }
  }
 
  func createBoard() {
    let board2 = modelsDidCreate(board: presenter.boardModel)

    board2.translatesAutoresizingMaskIntoConstraints = false
    board2.backgroundColor = .green
    self.view.addSubview(board2)
    NSLayoutConstraint.activate([
      board2.heightAnchor.constraint(equalTo: board2.widthAnchor, multiplier: 1),
      board2.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 30),
      board2.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -30),
      board2.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 100)
    ])
    board2.layer.masksToBounds = true
    board2.layer.borderWidth = 3
    board2.layer.borderColor = UIColor.lightGray.cgColor
    board2.tag = 100

    board3 = board2
    presenter.boardModel.width = board2.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).width
  }
  
  func createTiles() {
    var numberOfImage = 1

    tileWidth = board.frame.size.width / CGFloat(presenter.boardModel.size)
    
    guard var xCenter = xCenter, var yCenter = yCenter else { return }
    xCenter = tileWidth / 2
    yCenter = tileWidth / 2
    
    for _ in 0..<presenter.boardModel.size { //column
      for _ in 0..<presenter.boardModel.size { //row
        let tile = TileView(modelTile: Tile(tileWidth), tileWidth: tileWidth)
        let currentCenter = CGPoint(x: xCenter, y: yCenter)

        configureTiles(tile, numberOfImage, currentCenter)

        initialCentersOfTiles.append(currentCenter)
        presenter.initialTilesOrder.append(numberOfImage)
        presenter.tilesArray.insert(tile, at: presenter.tilesArray.count)
                
        board3.addSubview(tile)

        numberOfImage += 1
        xCenter += tileWidth
      }
      xCenter = tileWidth / 2
      yCenter += tileWidth
    }
    startingTilesPosition = Array(zip(presenter.initialTilesOrder, initialCentersOfTiles))
    let image = presenter.tilesArray[15]
    presenter.removeEmptyTileFromBoard(image as! UIImageView)
  }
  
  func createBoardAndTiles() {
    let boardWidth = board.frame.size.width
    tileWidth = boardWidth / CGFloat(presenter.boardModel.size)
    configureBoard(board: board)
        
    var numberOfImage = 1
    
    xCenter = tileWidth / 2
    yCenter = tileWidth / 2
    
    guard var xCenter = xCenter, var yCenter = yCenter else { return }

    for _ in 0..<presenter.boardModel.size { //column
      for _ in 0..<presenter.boardModel.size { //row
        let tile = TileView(modelTile: Tile(boardWidth / CGFloat(presenter.boardModel.size)), tileWidth: boardWidth / CGFloat(presenter.boardModel.size))
        let currentCenter = CGPoint(x: xCenter, y: yCenter)
        configureTiles(tile, numberOfImage, currentCenter)

        initialCentersOfTiles.append(currentCenter)
        presenter.initialTilesOrder.append(numberOfImage)
        presenter.tilesArray.insert(tile, at: presenter.tilesArray.count)
                
        board.addSubview(tile)

        numberOfImage += 1
        xCenter += tileWidth
      }
      xCenter = tileWidth / 2
      yCenter += tileWidth
    }
    startingTilesPosition = Array(zip(presenter.initialTilesOrder, initialCentersOfTiles))
    let image = presenter.tilesArray[15]
    presenter.removeEmptyTileFromBoard(image as! UIImageView)
  }
}

//MARK: Navigation Controller configuration
extension BoardVC {
  func configureNavigationController() {
    self.navigationItem.title = "Puzzle 15"
  }
}

//MARK: Configure labels
extension BoardVC {
  func configureLabels() {
    NSLayoutConstraint.activate([
      timerLabel.heightAnchor.constraint(equalToConstant: tileWidth / 3),
      timerLabel.widthAnchor.constraint(equalToConstant: tileWidth * 1.3),
      movesLabel.widthAnchor.constraint(equalToConstant: tileWidth * 1.3),
      movesLabel.heightAnchor.constraint(equalToConstant: tileWidth / 3),
    ])
  }
}

//MARK: Configure tiles
extension BoardVC {
  func configureTiles(_ tile: TileView, _ numberOfImage: Int, _ currentCenter: CGPoint) {
    tile.center = currentCenter
    tile.isUserInteractionEnabled = true
    tile.image = UIImage(named: "\(numberOfImage)")
    tile.layer.masksToBounds = true
    tile.layer.borderWidth = 1
    tile.layer.borderColor = UIColor.lightGray.cgColor
    tile.tag = numberOfImage
  }
}

//MARK: Configure board
extension BoardVC {
  func configureBoard(board: UIView) {
    board.layer.masksToBounds = true
    board.layer.borderWidth = 3
    board.layer.borderColor = UIColor.lightGray.cgColor
  }
}

//MARK: Add action and present alert
extension BoardVC {
  func addActionAndPresentAlert(alert: UIAlertController) {
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    self.present(alert, animated: true)
  }
}

//MARK: Timer
extension BoardVC {
  func createTimer() {
    timer = Timer.scheduledTimer(
      timeInterval: 1,
      target: self,
      selector: #selector(startTimer),
      userInfo: nil,
      repeats: true)
    }
  
  @objc func startTimer() {
    seconds += 1
    let min = seconds / 60
    let sec = seconds % 60

    let timerString = String.init(format: "%02d : %02d", min, sec)
    timerLabel.text = "\(timerString)"
  }
  
  func resetTimer() {
    timerLabel.text = "00 : 00"
    seconds = 0
  }
  
  func stopTimer() {
    timer.invalidate()
  }
}
