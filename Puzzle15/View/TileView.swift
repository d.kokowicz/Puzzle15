//
//  Tile.swift
//  Puzzle15
//
//  Created by Dominika Kokowicz on 23/05/2021.
//

import UIKit

final class TileView: UIImageView {
  var modelTile: Tile
  var imageTile: UIImage?
  
  
  init(modelTile: Tile, tileWidth: CGFloat) {
    self.modelTile = Tile(tileWidth)
    super.init(frame: CGRect(x: 0, y: 0, width: modelTile.width, height: modelTile.width))
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}


